db = db.getSiblingDB('books');

db.createCollection('authors', {capped: true, size: 100000});
db.createCollection('books', {capped: true, size: 100000});
db.createCollection('users', {capped: true, size: 100000});

db.authors.insertMany([

    {
        "_id": new ObjectId("64ff5c7b6aeb6d4d5acdf975"),
        "_class": "io.example.domain.model.Author",
        "bookIds": [
            new ObjectId("64ff5c7b6aeb6d4d5acdf976"),
            new ObjectId("64ff5c7b6aeb6d4d5acdf977")
        ],
        "createdAt": new ISODate("2023-09-11T18:29:15.813Z"),
        "fullName": "Andrzej Sapkowski",
        "genres": [],
        "modifiedAt": new ISODate("2023-09-11T18:29:15.968Z")
    },
    {
        "_id": new ObjectId("650034b66aeb6d4d5acdf97b"),
        "_class": "io.example.domain.model.Author",
        "bookIds": [
            new ObjectId("650034b66aeb6d4d5acdf97c"),
            new ObjectId("650034b66aeb6d4d5acdf97d")
        ],
        "createdAt": new ISODate("2023-09-11T18:29:15.813Z"),
        "fullName": "J.R.R.Tolkien",
        "genres": [],
        "modifiedAt": new ISODate("2023-09-11T18:29:15.968Z")
    }


]);



db.books.insertMany([
    {
        "_id": new ObjectId("64ff5c7b6aeb6d4d5acdf976"),
        "_class": "io.example.domain.model.Book",
        "authorIds": [
            new ObjectId("64ff5c7b6aeb6d4d5acdf975"),
        ],
        "createdAt": new ISODate("2023-09-11T18:29:15.892Z"),
        "genres": [],
        "hardcover": 0,
        "modifiedAt": new ISODate("2023-09-11T18:29:15.892Z"),
        "title": "Trylogia husycka"
    },
    {
        "_id": new ObjectId("64ff5c7b6aeb6d4d5acdf977"),
        "_class": "io.example.domain.model.Book",
        "authorIds": [
            new ObjectId("64ff5c7b6aeb6d4d5acdf975")
        ],
        "createdAt": new ISODate("2023-09-11T18:29:15.892Z"),
        "genres": [],
        "hardcover": 0,
        "modifiedAt": new ISODate("2023-09-11T18:29:15.892Z"),
        "title": "Sezon burz"
    },

    {
        "_id": new ObjectId("650034b66aeb6d4d5acdf97c"),
        "_class": "io.example.domain.model.Book",
        "authorIds": [
            new ObjectId("650034b66aeb6d4d5acdf97b")
        ],
        "createdAt": new ISODate("2023-09-12T09:51:50.392Z"),
        "genres": [],
        "hardcover": 0,
        "modifiedAt": new ISODate("2023-09-12T09:51:50.392Z"),
        "title": "The Fall of Gondolin"
    },
    {
        "_id": new ObjectId("650034b66aeb6d4d5acdf97d"),
        "_class": "io.example.domain.model.Book",
        "authorIds": [
            new ObjectId("650034b66aeb6d4d5acdf97b")
        ],
        "createdAt": new ISODate("2023-09-12T09:51:50.392Z"),
        "genres": [],
        "hardcover": 0,
        "modifiedAt": new ISODate("2023-09-12T09:51:50.392Z"),
        "title": "The Silmarillion"
    }
]);

db.users.insertMany([
    {
        "_id": new ObjectId("64ff5731d371f87a41c50c4a"),
        "_class": "io.example.domain.model.User",
        "authorities": [
            {
                "authority": "USER_ADMIN"
            }
        ],
        "createdAt": new ISODate("2023-09-11T18:06:41.957Z"),
        "enabled": true,
        "fullName": "ua",
        "modifiedAt": new ISODate("2023-09-12T10:48:12.130Z"),
        "password": "$2a$10$eiA5dKnoUk77EKXZhJvq7O3XBy5ECYupA0FCEm0gS58QSY6PoPcOS",
        "username": "ua"
    },
    {
        "_id": new ObjectId("64ff5732d371f87a41c50c4b"),
        "_class": "io.example.domain.model.User",
        "authorities": [
            {
                "authority": "AUTHOR_ADMIN"
            }
        ],
        "createdAt": new ISODate("2023-09-11T18:06:42.280Z"),
        "enabled": true,
        "fullName": "aa",
        "modifiedAt": new ISODate("2023-09-12T10:48:12.170Z"),
        "password": "$2a$10$dLrxYnLDd9XgjtAR3QqLXef5vOKh2RdyPRu1tGSEgI8Jie2FT2WZS",
        "username": "aa"
    },
    {
        "_id": new ObjectId("64ff5732d371f87a41c50c4c"),
        "_class": "io.example.domain.model.User",
        "authorities": [
            {
                "authority": "BOOK_ADMIN"
            }
        ],
        "createdAt": new ISODate("2023-09-11T18:06:42.414Z"),
        "enabled": true,
        "fullName": "ba",
        "modifiedAt": new ISODate("2023-09-12T10:48:12.183Z"),
        "password": "$2a$10$sQzsW/WL8/gt7vdnUJqAl.e4hamTabNZ3T5Xb3xlk0B6sIg.kJEae",
        "username": "ba"
    }
]);
