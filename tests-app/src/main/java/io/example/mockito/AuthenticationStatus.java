package io.example.mockito;

public enum AuthenticationStatus {
    AUTHENTICATED, NOT_AUTHENTICATED, ERROR
}
