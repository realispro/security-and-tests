package io.example.mockito;

import java.util.Objects;

public class Email {

    private String address;
    private String subject;
    private String body;
    private Format format;

    public Email(String address, String subject, String body) {
        this.address = address;
        this.subject = subject;
        this.body = body;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Format getFormat() {
        return format;
    }

    public void setFormat(Format format) {
        this.format = format;
    }

    @Override
    public String toString() {
        return "Email{" +
                "address='" + address + '\'' +
                ", subject='" + subject + '\'' +
                ", body='" + body + '\'' +
                ", format=" + format +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Email email = (Email) o;
        return Objects.equals(address, email.address) && Objects.equals(subject, email.subject) && Objects.equals(body, email.body) && format == email.format;
    }

    @Override
    public int hashCode() {
        return Objects.hash(address, subject, body, format);
    }
}
