package io.example.mockito;

public enum ServiceStatus {
    UP, DOWN, AUTHENTICATED
}
