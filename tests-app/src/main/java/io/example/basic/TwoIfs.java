package io.example.basic;

public class TwoIfs {

	public int twoIfs(int a, int b) {
		if (a > 0) {
			return 1;
		} else {
			System.out.println();
		}
		if (b > 0) {
			return 3;
		} else {
			return 4;
		}
	}
}
