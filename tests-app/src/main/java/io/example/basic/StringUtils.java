package io.example.basic;

import java.util.ArrayList;
import java.util.List;

public class StringUtils {

	public static boolean isNullOrBlank(String str) {
		return str == null || str.trim().length() == 0;
	}

	public static String getDefaultIfNull(final String str, final String defaultStr) {
		return str == null ? defaultStr : str;
	}

	public static Double convertToDouble(String str) {
		if (str == null) {
			return null;
		}
		return Double.valueOf(str);
	}

	public static String concat(String... strs) {
		String retVal = null;
		if (strs != null && strs.length > 0) {
			StringBuilder sb = new StringBuilder();
			for (String str : strs) {
				if (str != null) {
					sb.append(str);
				}
			}
			retVal = sb.toString();
		}
		return retVal;
	}

	public static String reverse(String s) {
		List<String> tempArray = new ArrayList<String>(s.length());
		for (int i = 0; i < s.length(); i++) {
			tempArray.add(s.substring(i, i + 1));
		}
		StringBuilder reversedString = new StringBuilder(s.length());
		for (int i = tempArray.size() - 1; i >= 0; i--) {
			reversedString.append(tempArray.get(i));
		}
		return reversedString.toString();
	}
}
