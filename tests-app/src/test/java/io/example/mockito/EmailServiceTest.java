package io.example.mockito;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class EmailServiceTest {


    @Mock
    DeliveryPlatform deliveryPlatform;

    @InjectMocks
    EmailService emailService;

    @Test
    void send() {

        emailService.send("You", "Hey", "Cheers", false);

        Email expectedEmail = new Email("You", "Hey", "Cheers");
        expectedEmail.setFormat(Format.TEXT_ONLY);

        Mockito.verify(deliveryPlatform).deliver(expectedEmail);

    }

    @Test
    void shouldReturnUp() {
        Mockito.when(deliveryPlatform.getServiceStatus()).thenReturn("OK");
        assertEquals(ServiceStatus.UP, emailService.checkServiceStatus());
    }

    @Test
    void shouldReturnDown() {
        Mockito.when(deliveryPlatform.getServiceStatus()).thenReturn("ANY");
        assertEquals(ServiceStatus.DOWN, emailService.checkServiceStatus());
    }

    @Test
    void shouldAuthenticate() {

        Mockito.when(deliveryPlatform.authenticate(any()))
                .thenReturn(AuthenticationStatus.AUTHENTICATED);
        assertTrue(emailService.authenticatedSuccessfully(
                        new Credentials("any", "any", "any")));
    }

    @Test
    void shouldReturnFalseOnAuthenticate() {

        Mockito.when(deliveryPlatform.authenticate(any()))
                .thenReturn(AuthenticationStatus.NOT_AUTHENTICATED);
        assertFalse(emailService.authenticatedSuccessfully(
                new Credentials("any", "any", "any")));
    }
}