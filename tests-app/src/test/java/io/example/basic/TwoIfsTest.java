package io.example.basic;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class TwoIfsTest {

    @ParameterizedTest
    @MethodSource("ifsArgs")
    void shouldReturnExpected(int a, int b, int expected) {

        TwoIfs ifs = new TwoIfs();

        int result = ifs.twoIfs(a, b);
        assertEquals(expected, result);
    }

    static Stream<Arguments> ifsArgs(){
        return Stream.of(
                Arguments.of(2,0,1),
                Arguments.of(0,2,3),
                Arguments.of(0,0,4)
        );
    }
}