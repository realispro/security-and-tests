package io.example.basic;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    Calculator calculator = null;

    @BeforeEach
    void initEach(){
        calculator = new Calculator();
    }

    @Test
    void shouldCalculateSumProperly() {
        String numbers = "102 303 1024";
        int sum = Assertions.assertDoesNotThrow(()-> calculator.calculateSum(numbers));
        //int sum = calculator.calculateSum(numbers);
        Assertions.assertEquals(1429, sum, "unexpected value");
    }

    @Test
    void shouldThrowOnFloatNumber(){
        String numbers = "102 3.03 1024";
        assertThrows(
                NumberFormatException.class,
                ()->calculator.calculateSum(numbers));
    }

    @Test
    void shouldReturnZeroEmptyString(){
        String numbers = "";
        int sum = assertDoesNotThrow(()->calculator.calculateSum(numbers));
        assertEquals(0, sum);
    }
}