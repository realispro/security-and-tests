package io.example.configuration;

import static java.util.Optional.ofNullable;

import io.example.domain.model.User;
import org.bson.types.ObjectId;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

@Configuration
@EnableMongoAuditing
public class MongoConfig {

  @Bean
  public AuditorAware<ObjectId> auditorProvider() {
    return () -> {
      User user = null; // TODO get user with SecurityContextHolder
      return ofNullable(user).map(User::getId);
    };
  }
}
