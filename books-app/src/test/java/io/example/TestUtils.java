package io.example;

import io.example.domain.model.Author;
import io.example.domain.model.Book;
import org.bson.Document;

import java.time.LocalDateTime;
import java.util.Map;

public class TestUtils {

    public static Author authorDocument(String fullName){
        Author author = new Author();
        author.setFullName(fullName);
        author.setCreatedAt(LocalDateTime.now());
        author.setModifiedAt(LocalDateTime.now());
        return author;
    }

    public static Book bookDocument(String title){

        Book book = new Book();
        book.setTitle(title);
        book.setCreatedAt(LocalDateTime.now());
        book.setModifiedAt(LocalDateTime.now());
        return book;
    }
}
