package io.example.exclude;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(scanBasePackages="io.example", exclude=SecurityAutoConfiguration.class)
public class UnsecureApplication {

    public static void main(String[] args) {
        SpringApplication.run(UnsecureApplication.class, args);
    }
}
