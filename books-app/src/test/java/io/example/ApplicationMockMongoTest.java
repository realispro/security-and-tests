package io.example;


import io.example.domain.model.Author;
import io.example.domain.model.Book;
import io.example.repository.AuthorRepo;
import io.example.repository.BookRepo;
import io.restassured.RestAssured;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = Application.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@AutoConfigureDataMongo
public class ApplicationMockMongoTest {

    @LocalServerPort
    private int port;

    @Autowired
    BookRepo bookRepo;

    @Autowired
    AuthorRepo authorRepo;

    @Test
    public void givenSecurity(@Autowired final MongoTemplate mongoTemplate) {
        assertThat(mongoTemplate.getDb()).isNotNull();

        Author stanislawLem = TestUtils.authorDocument("Stanislaw Lem");
        authorRepo.save(stanislawLem);
        Book cyberiada = TestUtils.bookDocument("Cyberiada");
        bookRepo.save(cyberiada);
        ObjectId id = (ObjectId) cyberiada.getId();
        System.out.println("id=" + id);

        int statusCode =
                RestAssured
                .get("http://localhost:" + port + "/api/book/" + id.toString())
                .statusCode();
        assertEquals(HttpStatus.OK.value(), statusCode);
    }
}
